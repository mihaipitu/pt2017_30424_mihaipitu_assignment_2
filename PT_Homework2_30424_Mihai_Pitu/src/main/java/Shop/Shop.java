package Shop;
import java.util.Date;
import java.util.Random;
import GUI.OutputFileText;

public class Shop extends Thread
{
	public int noCheckouts;
	public int openTime;
	public int closedTime;
	public int minServiceTime;
	public int maxServiceTime;
	public int minArrivalTime;
	public int maxArrivalTime;
	public static long simulationTime;
	
	public int sumEmptyQueueTime=0;
	public int noClients = 0;
	public Checkout[] checkouts;
	public int sumServiceTime = 0;
	public float meanServiceTime = 0;
	public float meanArrivalTime = 0;
	public OutputFileText out = new OutputFileText();
	
	public Shop(int noCheckouts,int minServiceTime,int maxServiceTime,int openTime,int closedTime,int minArrivalTime,int maxArrivalTime)
	{
		this.noCheckouts = noCheckouts;
		this.minServiceTime = minServiceTime;
		this.maxServiceTime = maxServiceTime;
		this.openTime = openTime;
		this.closedTime = closedTime;
		this.minArrivalTime = minArrivalTime;
		this.maxArrivalTime = maxArrivalTime;
		int i;
		checkouts = new Checkout[noCheckouts+1];
		for(i = 0; i<noCheckouts; i++)
			checkouts[i] = new Checkout();
		simulationTime = closedTime - openTime;
	}
	
	public void run()
	{
		int waitTime;
		System.out.println("The shop is open.");
		while(simulationTime>0)
		{
			Random rand = new Random();
			Checkout check;
			noClients++;
			int emptyCheckOuts = getNoOfEmptyCheckouts();
			Customer c = new Customer(noClients, new Date(), rand.nextInt(maxServiceTime-minServiceTime+1) + minServiceTime);
			check = checkouts[minimumQueue()];
			check.addCustomer(c);
			sumServiceTime += c.getServiceTime();
			System.out.println("Customer " + noClients + " is at checkout no " + (minimumQueue()+1) + " at: " + c.getArrivalTime() + ". He stayed at the queue for " + c.getServiceTime() + " minutes.\n");
			waitTime = rand.nextInt(maxArrivalTime-minArrivalTime+1) + minArrivalTime;
			try
			{
				sleep(waitTime*100);
			}
			catch(InterruptedException e)
			{
				System.out.println(Shop.class.getName());
			}
			if(emptyCheckOuts!=0)
			{
				sumEmptyQueueTime += waitTime*(emptyCheckOuts-1);
			}
			simulationTime-=waitTime;
		}
		System.out.println("The shop is closed.");
		stop();
		meanServiceTime = meanServiceTime();
		meanArrivalTime = meanEmptyQueue();
	}
	
	public int minimumQueue()
	{
		int sum;
		int min = noClients * maxServiceTime;
		Checkout check;
		Customer c;
		int i,j;
		int queueNo = 0;
		
		for(i = 0 ; i<noCheckouts;i++)
		{
			sum = 0;
			check = checkouts[i];
			for(j=0; j<check.getQueueSize();j++)
			{
				if(!check.queue.isEmpty())
				{
					c = (Customer) check.queue.elementAt(j);
					sum += c.getServiceTime();
				}
			}
			if(min>sum)
			{
				min = sum;
				queueNo = i;
			}
		}
		return queueNo;
	}
		
	
	public int getNoOfEmptyCheckouts()
	{
		int nr = 0;
		int i;
		for(i=0 ;i<noCheckouts;i++)
			if(checkouts[i].queue.isEmpty())
				nr++;
		return nr;
	}
	
	public float meanServiceTime()
	{
		float mean = 0;
		mean = (float) sumServiceTime / noClients;
		return mean;
	}
	
	public float meanEmptyQueue()
	{
		float mean = 0;
		mean = (float) sumEmptyQueueTime / noCheckouts ;
		return mean;
	}
}
