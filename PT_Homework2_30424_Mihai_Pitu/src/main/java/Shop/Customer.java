package Shop;
import java.util.Date;

public class Customer {
	private int id;
	private Date arrivalTime;
	private int serviceTime;
	
	public Customer(int id,Date arrivalTime,int serviceTime)
	{
		this.id = id;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}
	
	public int getId()
	{
		return this.id;
	}

	public Date getArrivalTime()
	{
		return this.arrivalTime;
	}
	
	public int getServiceTime()
	{
		return this.serviceTime;	
	}
}
