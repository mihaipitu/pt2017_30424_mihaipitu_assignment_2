package Shop;
import java.util.Date;
import java.util.Vector;
import GUI.OutputFileText;


public class Checkout extends Thread
{
	Vector<Customer> queue;
	
	public OutputFileText out = new OutputFileText();
	public Checkout()
	{
		queue = new Vector<Customer>();
	}
	
	public void addCustomer(Customer c)
	{
		queue.addElement(c);
	}
	
	public void removeCustomer()
	{
		if(!queue.isEmpty())
		{
			queue.remove(0);
		}
	}
	
	public int getQueueSize()
	{
		return queue.size();
	}
	
	public void run()
	{
		Customer c;
		while(Shop.simulationTime >0)
		{
			if(!queue.isEmpty())
			{
				c = (Customer) queue.elementAt(0);
				try
				{
					sleep(c.getServiceTime()*100);
				}
				catch (InterruptedException e)
				{
					System.out.println(Checkout.class.getName());
				}
				System.out.println("Customer " + c.getId() + " leaves at " + new Date() +".\n");
				Shop.simulationTime -= c.getServiceTime();
				removeCustomer();	
			}
		}
		stop();
	}
	
	public int sumServiceTime()
	{
		int sum = 0;
		int i;
		for(i = 0; i<getQueueSize();i++)
			sum = sum + ((Customer) queue.elementAt(i)).getServiceTime();
		return 0;
	}
}