package GUI;
import java.io.File;
import java.io.PrintStream;
import java.io.IOException;


public class OutputFileText {
	File outputFile = new File("output.txt");
	public OutputFileText()
	{
		setOutputToFile();
	}
	
	public void setOutputToFile()
	{
	try{
		if(!outputFile.exists())
		{
			outputFile.createNewFile();
		}
		PrintStream output = new PrintStream(outputFile);
		System.setOut(output);
	}catch(IOException e){
		System.out.println(OutputFileText.class.getName());
	}
	
	
}
	
	
}
