package GUI;
import java.awt.Desktop;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import Shop.Shop;

import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;

public class GUI {

	private JFrame frmQueueSimulator;
	private JTextField noQueues;
	private JTextField minServiceTime;
	private JTextField maxServiceTime;
	private JTextField minArrivalInterval;
	private JTextField maxArrivalInterval;
	public JTextArea outputArea;
	private Shop shop;
	private OutputFileText out = new OutputFileText();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmQueueSimulator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmQueueSimulator = new JFrame();
		frmQueueSimulator.setTitle("Queue Simulator");
		frmQueueSimulator.setBounds(100, 100, 386, 541);
		frmQueueSimulator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmQueueSimulator.getContentPane().setLayout(null);
		
		noQueues = new JTextField(10);
		noQueues.setBounds(214, 12, 86, 20);
		frmQueueSimulator.getContentPane().add(noQueues);
		noQueues.setColumns(10);
		
		JLabel lblNumberOfQueues = new JLabel("Number of queues");
		lblNumberOfQueues.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNumberOfQueues.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNumberOfQueues.setBounds(10, 14, 194, 14);
		frmQueueSimulator.getContentPane().add(lblNumberOfQueues);
		
		minServiceTime = new JTextField(10);
		minServiceTime.setBounds(214, 42, 86, 20);
		frmQueueSimulator.getContentPane().add(minServiceTime);
		minServiceTime.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Minimum Service Time");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 44, 194, 14);
		frmQueueSimulator.getContentPane().add(lblNewLabel);
		
		maxServiceTime = new JTextField(10);
		maxServiceTime.setBounds(214, 73, 86, 20);
		frmQueueSimulator.getContentPane().add(maxServiceTime);
		maxServiceTime.setColumns(10);
		
		JLabel lblMaximumServiceTime = new JLabel("Maximum Service Time");
		lblMaximumServiceTime.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMaximumServiceTime.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblMaximumServiceTime.setBounds(10, 75, 194, 14);
		frmQueueSimulator.getContentPane().add(lblMaximumServiceTime);
		
		minArrivalInterval = new JTextField(10);
		minArrivalInterval.setBounds(214, 104, 86, 20);
		frmQueueSimulator.getContentPane().add(minArrivalInterval);
		minArrivalInterval.setColumns(10);
		
		JLabel lblMinimumArrivalInterval = new JLabel("Minimum Arrival Interval");
		lblMinimumArrivalInterval.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMinimumArrivalInterval.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblMinimumArrivalInterval.setBounds(10, 107, 194, 14);
		frmQueueSimulator.getContentPane().add(lblMinimumArrivalInterval);
		
		maxArrivalInterval = new JTextField(10);
		maxArrivalInterval.setBounds(214, 135, 86, 20);
		frmQueueSimulator.getContentPane().add(maxArrivalInterval);
		maxArrivalInterval.setColumns(10);
		
		JLabel lblMaximumArrivalInterval = new JLabel("Maximum Arrival Interval");
		lblMaximumArrivalInterval.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblMaximumArrivalInterval.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMaximumArrivalInterval.setBounds(10, 138, 194, 14);
		frmQueueSimulator.getContentPane().add(lblMaximumArrivalInterval);
		
		final JSpinner openMinutes = new JSpinner();
		openMinutes.setModel(new SpinnerNumberModel(0, 0, 59, 1));
		openMinutes.setBounds(258, 166, 42, 20);
		frmQueueSimulator.getContentPane().add(openMinutes);
		
		JLabel lblMinutes = new JLabel("minutes");
		lblMinutes.setHorizontalAlignment(SwingConstants.LEFT);
		lblMinutes.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblMinutes.setBounds(310, 169, 58, 14);
		frmQueueSimulator.getContentPane().add(lblMinutes);
		
		JLabel label_1 = new JLabel("minutes");
		label_1.setHorizontalAlignment(SwingConstants.LEFT);
		label_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label_1.setBounds(310, 45, 58, 14);
		frmQueueSimulator.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("minutes");
		label_2.setHorizontalAlignment(SwingConstants.LEFT);
		label_2.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label_2.setBounds(310, 76, 58, 14);
		frmQueueSimulator.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("minutes");
		label_3.setHorizontalAlignment(SwingConstants.LEFT);
		label_3.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label_3.setBounds(310, 107, 58, 14);
		frmQueueSimulator.getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("minutes");
		label_4.setHorizontalAlignment(SwingConstants.LEFT);
		label_4.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label_4.setBounds(310, 138, 58, 14);
		frmQueueSimulator.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("minutes");
		label_5.setHorizontalAlignment(SwingConstants.LEFT);
		label_5.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label_5.setBounds(310, 199, 58, 14);
		frmQueueSimulator.getContentPane().add(label_5);
		
		final JSpinner closedMinutes = new JSpinner();
		closedMinutes.setModel(new SpinnerNumberModel(0, 0, 59, 1));
		closedMinutes.setBounds(258, 197, 42, 20);
		frmQueueSimulator.getContentPane().add(closedMinutes);
		
		JLabel lblHours = new JLabel("hours");
		lblHours.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblHours.setBounds(202, 169, 46, 14);
		frmQueueSimulator.getContentPane().add(lblHours);
		
		JLabel label_6 = new JLabel("hours");
		label_6.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label_6.setBounds(202, 200, 46, 14);
		frmQueueSimulator.getContentPane().add(label_6);
		
		final JSpinner openHours = new JSpinner();
		openHours.setModel(new SpinnerNumberModel(0, 0, 23, 1));
		openHours.setBounds(150, 166, 42, 20);
		frmQueueSimulator.getContentPane().add(openHours);
		
		final JSpinner closedHours = new JSpinner();
		closedHours.setModel(new SpinnerNumberModel(0, 0, 23, 1));
		closedHours.setBounds(150, 197, 42, 20);
		frmQueueSimulator.getContentPane().add(closedHours);
		
		JLabel lblNewLabel_1 = new JLabel("Open Time");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(10, 169, 130, 14);
		frmQueueSimulator.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Closed Time");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(10, 200, 130, 14);
		frmQueueSimulator.getContentPane().add(lblNewLabel_2);
		
		outputArea = new JTextArea();
		outputArea.setEditable(false);
		outputArea.setFont(new Font("Times New Roman", Font.BOLD, 14));
		outputArea.setBounds(10, 373, 350, 118);
		frmQueueSimulator.getContentPane().add(outputArea);
		
		JButton btnStartSimulation = new JButton("Start Simulation");
		btnStartSimulation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				outputArea.setText(null);
				String nrQueue = "0";
				String minServTime = "0";
				String maxServTime = "0";
				String minArivTime = "0";
				String maxArivTime = "0";
				
				nrQueue = noQueues.getText();
				minServTime = minServiceTime.getText();
				maxServTime = maxServiceTime.getText();
				minArivTime = minArrivalInterval.getText();
				maxArivTime = maxArrivalInterval.getText();
				int openMin = (Integer)openMinutes.getValue();
				int openHour = (Integer)openHours.getValue();
				int closedMin = (Integer)closedMinutes.getValue();
				int closedHour = (Integer)closedHours.getValue();
				
				int nrQueues = 0;
				int minService = 0;
				int maxService = 0;
				int minArrival = 0;
				int maxArrival = 0;
				int openInterval = 0;
				int closedInterval = 0;
				
				nrQueues = Integer.parseInt(nrQueue);
				minService = Integer.parseInt(minServTime);
				maxService = Integer.parseInt(maxServTime);
				minArrival = Integer.parseInt(minArivTime);
				maxArrival = Integer.parseInt(maxArivTime);
	
				
				openInterval = openMin + 60 * openHour;
				closedInterval = closedMin + 60 * closedHour;
				if(minService<=0 || maxService<=0 || minArrival<=0 || maxArrival<=0 || nrQueues<=0)
				{
					JOptionPane.showMessageDialog(null, "Error - queue no/service time interval/arrival time interval should be greater than 0.\n Do it again!");
				}
				else
				{
					if(openInterval>closedInterval)
					{
						JOptionPane.showMessageDialog(null, "Error - wrong interval.\n Do it again!");
					}
					else
					{
						if(maxService<minService)
						{
							JOptionPane.showMessageDialog(null, "Error - maxService>minService.\n Do it again!");
						}
						else
						{
							if(maxArrival<minArrival)
							{
								JOptionPane.showMessageDialog(null, "Error - maxArrivalTime>minArrivalTime.\n Do it again!");
							}
							else
							{
								outputArea.append("The simulation has started.\n");
								shop = new Shop(nrQueues,minService,maxService,openInterval,closedInterval,minArrival,maxArrival);
								shop.start();
								for(int i=0;i<nrQueues;i++)
								{
									shop.checkouts[i].start();
								}
								outputArea.append("The simulation has finished.\n");
							}
						}
					}
				}
			}
		});
		btnStartSimulation.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnStartSimulation.setBounds(87, 228, 213, 41);
		frmQueueSimulator.getContentPane().add(btnStartSimulation);
		
		JButton btnNewButton = new JButton("Open Queue Activity");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().open(out.outputFile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnNewButton.setBounds(87, 272, 213, 41);
		frmQueueSimulator.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Show mean values of queues");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				outputArea.setText(null);
				float meanServiceTime = 0;
				float meanArrivalTime = 0;
				
				meanServiceTime = shop.meanServiceTime();
				meanArrivalTime = shop.meanEmptyQueue();
				
				outputArea.append("Mean of service time: " + meanServiceTime + " seconds.\n");
				outputArea.append("Mean of arrival time: " + meanArrivalTime + " seconds.\n");
				outputArea.append("There have been " + shop.noClients + " clients today.\n");
				outputArea.append("The total service time: " + shop.sumServiceTime + " minutes.\n");
			}
		});
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnNewButton_1.setBounds(87, 318, 213, 44);
		frmQueueSimulator.getContentPane().add(btnNewButton_1);
	}
}
